import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import AccountPage from "../pages/AccountPage";
import ProjectsPage from "../pages/ProjectsPage";
import ProjectPage from "../pages/ProjectPage";
import UsersPage from "../pages/admin/UsersPage";
import NotFoundPage from "../pages/NotFoundPage";
import Layout from "../components/layouts/Layout";
import PrivateRoute from "./PrivateRoute";

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route exact path="/" element={<HomePage />} />
          <Route exact path="/login" element={<LoginPage />} />
          <Route exact path="/register" element={<RegisterPage />} />
          <Route exact path="/account" element={<PrivateRoute />}>
            <Route exact path="/account" element={<AccountPage />} />
          </Route>
          <Route exact path="/projects" element={<PrivateRoute />}>
            <Route exact path="/projects" element={<ProjectsPage />} />
          </Route>
          <Route exact path="/project/:projectId" element={<PrivateRoute />}>
            <Route exact path="/project/:projectId" element={<ProjectPage />} />
          </Route>
          <Route exact path="/admin/users" element={<PrivateRoute />}>
            <Route exact path="/admin/users" element={<UsersPage />} />
          </Route>

          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};
export default AppRouter;
